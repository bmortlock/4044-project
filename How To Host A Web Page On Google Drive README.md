# Welcome to the How to host a webpage on Google Drive README

## Please see below steps on how to host your webpage externally to the public

## Q: How do I host a Web Page on Google Drive?
+  1: Create a folder in Google Drive and set the sharing permissions to "Public on the Web."
+  2: Upload the HTML, Javascript and CSS Files for your web page to the new created folder.
+  3: Select the HTML file, open it and click the "preview" button in the toolbar.
+  4: Share the URL (it will look like www.googledrive.com/host/....) and anyone can view your web page!

## Q: Is there a link to a guide which has a video? 
+  A: Yes, please vist ; " https://www.bettercloud.com/monitor/the-academy/how-to-host-a-web-page-on-google-drive/ "

# The End!