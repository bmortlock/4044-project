from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField, TextField, SelectField, RadioField, TextAreaField, IntegerField
from wtforms.validators import DataRequired, Length, Email, EqualTo

class LoginForm(FlaskForm):
    username = StringField('username', validators=[DataRequired()])
    password = StringField('password', validators=[DataRequired()])
    remember_me = BooleanField('Remember Me')
    submit = SubmitField('sign in')

class JobSearch(FlaskForm):
    role_name = StringField('role_name' , validators=[DataRequired()])
    submit = SubmitField('Search!')

class RegisterForm(FlaskForm):
    username = StringField('Username', validators = [Length(min=4, max=25)])
    email = StringField('Email Address', validators = [Length(min=6, max=35), Email()])
    password = PasswordField('New Password', validators = [DataRequired(), EqualTo('confirm', message='Passwords must match')])
    confirm = PasswordField('Repeat Password')
    submit = SubmitField('Register')

class ContactForm(FlaskForm):
    name = TextField('Name Of Student',validators=[DataRequired('Please enter your name.')])
    gender = RadioField('Gender', choices = [('M','Male'),('F','Female')])
    address = TextAreaField('Address')
    email = TextField('Email',validators=[DataRequired('Please enter your email address. '), Email('Please enter a valid email address. ')])
    age = IntegerField('age')
    role = SelectField('Role', choices = [('SE','software engineering'), ('SD', 'software developer'), ('SA','system analyst')])
    submit = SubmitField('Send')