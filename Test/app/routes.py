from flask import render_template, flash, redirect, url_for,  request
from app import app
from app.forms import LoginForm
from app.forms import JobSearch
from flask_wtf.csrf import CSRFError
from app import csrf
from .forms import RegisterForm
from .forms import ContactForm
from flaskext.mysql import MySQL

mysql = MySQL()
mysql.init_app(app)

 # connect to database
conn = mysql.connect()

    # create a database cursor
cursor = conn.cursor()

def get_rows():
    """ gets query results from cursor as
        list of dicts
    """
    fields = [i[0] for i in cursor.description]
    return [ dict(zip(fields, row)) for row in list(cursor.fetchall())]

@app.route('/')
@app.route('/index')
def index():
    
    return render_template('index.html', title='Home')


@app.route('/contactnew')
def contact():
    cursor.execute("SELECT username, Departmental_Subject, email FROM contact")
    contacts = get_rows()
    return render_template('contactnew.html', title='Contact Form', data=contacts)
    
 
@app.route('/jobsearch')
def job_search():
    cursor.execute("SELECT id, ABS(DATEDIFF(posting_date, CURDATE())) AS days_since_posting, \
            closing_date, organisation, title,longitude, latitude, description, job_level FROM job ORDER BY days_since_posting ASC")
    jobs = get_rows()
    return render_template('jobsearch.html', title='job search', data=jobs)

@app.route('/login')
def login():
    
    return render_template('login.html', title='login')
   
def check_csrf():
    if not is_oauth(request):
        csrf.protect();


 
