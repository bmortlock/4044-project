from flask import Flask
from config import Config
from flask_wtf.csrf import CsrfProtect
from flaskext.mysql import MySQL

app = Flask(__name__)
app.config.from_object(Config)
csrf = CsrfProtect(app)

from app import routes
