Cross site request forgery - Execute unwanted actions

CSRF attack is called Cross-Site Request Forgery

CSRF attack is a major attack on web application in which
attackers execute unwanted actions to trick users in web
applications

------------------------------------------------------------------------
SQL injection -  Attacks on web database

In SQL attack, attackers insert malicious scripts in the database
to get sensitive information

SQL injection attack can provide the attacker with sensitive
information such as username, password, phone number,
address, etc. 

------------------------------------------------------------------------
XSS attack - Execute malicious scripts in victim�s browser

XSS attack is a type of attack where the attackers insert
malicious commands in the web application.


Escaping process

It means if application gets any data, ensure it before
rendering process
Escape all type of code techniques (HTML link, URL, and
JavaScript) from application areas 

Sanitizing Process

It means when application allow users to input HTML, then web
application should use the sanitizing process to allow only
trusted inputs from users

------------------------------------------------------------------------
Broke authentication and session management - Session hijacking

------------------------------------------------------------------------
Insecure direct object reference - URL manipulation
------------------------------------------------------------------------
Security misconfiguration - Server maintenance problem

------------------------------------------------------------------------
Flask app secret key
------------------------------------------------------------------------
------------------------------------------------------------------------
Python shell
------------------------------------------------------------------------
>>> import os
>>> os.urandom(64) <-- this is the code you type
'q\xe3g\xa9\xccE\xefS\x88\x9ei5\x14\xb2\x80\x9fl\xdb>i\xcf=fk\xf1^c\xe
f\x8c1z\x9a\xd6\xae\x08S\xfc\xe0\xe0\x03\x93\xe6\xdc\xf9\xf0\x12q&\xf7
\x07\x10\x14\xc5w\x92\xed\xc1\xa4\xe8(\x06\xbc\x98A'

------------------------------------------------------------------------
Windows cd in Venv
------------------------------------------------------------------------
set SECRET_KEY='q\xe3g\xa9\xccE\xefS\x88\x9ei5\x14\xb2\x80\x9fl\xdb>i\xcf=
fk\xf1^c\xef\x8c1z\x9a\xd6\xae\x08S\xfc\xe0\xe0\x03\x93\xe6\xdc\xf9\xf
0\x12q&\xf7\x07\x10\x14\xc5w\x92\xed\xc1\xa4\xe8(\x06\xbc\x98A' 