# Welcome to the How to Add Latitue and Longitude to the Jobs Table within SQL README

## Please see below steps on how to Add Latitue and Longitude to the Jobs Table

## Q: Add Latitue and Longitude to the Jobs Table?
+  1: Within your database ensure you are connected and within your LTU_MYFUTRE .DB
+  2: Once you're in the Database begin to execute the following statment. 
+
+        ALTER TABLE `ltu_myfuture`.`job` 
+        ADD COLUMN `Latitude` DECIMAL(10,8) NOT NULL AFTER `job_level`,
+        ADD COLUMN `Longitude` DECIMAL(11,8) NOT NULL AFTER `Latitude`;


## You have now managed to add two new fields within the Jobs table. 


# The End!