-- HERE I AM CREATING MY NEWDATABASE -----------------------------------------------
CREATE DATABASE LTU_MYFUTURE;


-- HERE I AM SWITCHING MY DB --I WILL BE --USING ltu_test instead ofMASTER ---------- 
USE LTU_MYFUTURE; 


-- -------------- ABOVE I AM CREATING --NEWTABLES -------------------

CREATE TABLE Job ( 
id INT AUTO_INCREMENT, 
posting_date DATE,
closing_date DATE, 
organisation VARCHAR (100) NOT NULL , 
title VARCHAR (100) NOT NULL , 
description text NOT NULL,
job_level varchar(10),
PRIMARY KEY(id)
) engine=InnoDB;
-- -----------------------------------------------------------------------

CREATE TABLE User (
    username VARCHAR(12) UNIQUE NOT NULL,
    password VARCHAR(50) NOT NULL,
    email VARCHAR(200) UNIQUE NOT NULL,
    user_level VARCHAR(10),
    PRIMARY KEY (username)
)  ENGINE=INNODB;
    
-- ---------------------------------------------------------------------
 
CREATE TABLE Tag (
    username VARCHAR(12) NOT NULL,
    job_id INT NOT NULL,
    PRIMARY KEY (username , job_id),
    FOREIGN KEY (username)
        REFERENCES User (username),
    FOREIGN KEY (job_id)
        REFERENCES Job (id)
)  ENGINE=INNODB;

-- -----------------------------------------------------------------------

CREATE TABLE Location (
    Address1 VARCHAR(30),
    Address2 VARCHAR(30),
    Town VARCHAR(30),
    County VARCHAR(30),
    Country VARCHAR(30),
    Postal_Code VARCHAR(30)
)  ENGINE=INNODB;


-- ALTERING LOCATION_TABLE ---------------------------------------------------

ALTER TABLE `LTU_MYFUTURE`.`location` 
CHANGE COLUMN `Address1` `Address1` VARCHAR(30) NOT NULL ,
CHANGE COLUMN `Address2` `Address2` VARCHAR(30) NOT NULL ,
CHANGE COLUMN `Town` `Town` VARCHAR(30) NOT NULL ,
CHANGE COLUMN `County` `County` VARCHAR(30) NOT NULL ,
CHANGE COLUMN `Country` `Country` VARCHAR(30) NOT NULL ,
CHANGE COLUMN `Postal_Code` `Postal_Code` VARCHAR(30) NOT NULL ;

-- ----------------------------------------------------------------------------------------------------
 
CREATE TABLE Contact (
    username VARCHAR(12) UNIQUE NOT NULL,
    Departmental_Subject VARCHAR(50) NOT NULL,
    email VARCHAR(200) UNIQUE NOT NULL,
    PRIMARY KEY (username)
)  ENGINE=INNODB;


-- -------------BELOW I AM GOING --TOINSERT --NEW --DATA --INTO MY --TABLES ------------
  INSERT INTO User (username, password, email, user_level) 
  VALUES 		("1806704", "Bilal", "1806704@leedstrinity.ac.uk", "Level 4"),
				("1806471", "Brandon", "1806471@leedstrinity.ac.uk", "Level 4"),
				("1804547", "Barnaby", "1804547@leedstrinity.ac.uk", "Level 4");
  
-- ------------------------------------------------------------------
INSERT INTO Job (posting_date, closing_date, organisation, title, description, job_level)
 VALUES        ("2019-02-04", "2019-02-25", "Glawning" , "Marketing Assistant" , "Involvement in many aspects in the day to day running of the business – predominantly based around marketing.", "Level 4"),
			   ("2019-02-11",  "2019-02-25", "Yorkshire Montessori", "Nursery Assistant", "Promoting positive relationships, promoting the health, safety and well-being of the children and support children’s development, learning and play.", "Level 4"),
			   ("2019-01-28", "2019-02-25", "One You Leeds", "Health Champion", "You 
			   
UPDATE Job SET Latitude = '53.965388', Longitude = '-1.306408' WHERE id = '1' 
UPDATE Job SET Latitude = '53.965388', Longitude = '-1.306408' WHERE id = '2' 
UPDATE Job SET Latitude = '53.816269', Longitude = '-1.577284' WHERE id = '3' 
UPDATE Job SET Latitude = '53.771849', Longitude = '-1.51745' WHERE id = '4' 		   
			   
			   
-- -------------------------------------------------------------------------------------------------------
INSERT INTO Contact (username, departmental_subject, email) 
VALUES 			("S.Harriet", "Computer Science", "S.Harriet@leedstrinity.ac.uk"),
				("G.Barnes", "Business", "G.Barnes@leedstrintiy.ac.uk"),
                ("B.Johnson", "Marketing" , "B.Johnson@leedstrinity.ac.uk"), 
                ("G.Davidson", "Media" , "G.Davidson@leedstrinity.ac.uk"),
                ("J.McGraph" , "Journalism", "J.McGraph@leedstrinity.ac.uk");
